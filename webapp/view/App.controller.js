sap.ui.controller("demo.view.App", {

	onInit: function () {
		this.oModel = new sap.ui.model.json.JSONModel({
			newTodo: "",
			todos: [
				{
					title: "Start this app",
					completed: true
				},
				{
					title: "Learn OpenUI5",
					completed: false
				}
			],
			someCompleted: true,
			completedCount: 1
		});
	},

	/**
	 * Navigates to another page
	 * @param {string} pageId The id of the next page
	 * @param {sap.ui.model.Context} context The data context to be applied to the next page (optional)
	 */
	to: function (pageId, context) {

		var app = this.getView().app;

		// load page on demand
		var menu = ("Menu" === pageId);
		if (app.getPage(pageId, menu) === null) {
			var page = sap.ui.view({
				id: pageId,
				viewName: "demo.view." + pageId,
				type: "XML"
			});
			page.getController().nav = this;
			app.addPage(page, menu);
			jQuery.sap.log.info("app controller > loaded page: " + pageId);
		}

		// show the page
		app.to(pageId);

		// set data context on the page
		if (context) {
			var page = app.getPage(pageId);
			page.setBindingContext(context);
		}
	},

	/**
	 * Navigates back to a previous page
	 * @param {string} pageId The id of the next page
	 */
	back: function (pageId) {
		this.getView().app.backToPage(pageId);
	}

});
