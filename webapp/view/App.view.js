sap.ui.jsview("demo.view.App", {

	getControllerName: function () {
		return "demo.view.App";
	},

	createContent: function (oController) {

		// to avoid scroll bars on desktop the root view must be set to block display
		this.setDisplayBlock(true);

		// create app
		this.app = new sap.m.App();

		// load the music page
		var menu = sap.ui.xmlview("Menu", "demo.view.Menu");
		menu.getController().nav = this.getController();
		this.app.addPage(menu, true);

		// load the music page
		var music = sap.ui.xmlview("Music", "demo.view.Music");
		music.getController().nav = this.getController();
		this.app.addPage(music, false);

		// load the todolist page
		var todo = sap.ui.xmlview("Todo", "demo.view.Todo");
		todo.getController().nav = this.getController();
		this.app.addPage(todo, false);

/*		// load the sopraWS page
		var sopraws = sap.ui.xmlview("Sopraws", "demo.view.Sopraws");
		this.app.addPage(sopraws, false);*/

		return this.app;
	}
});
