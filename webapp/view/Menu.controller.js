/**
 * Created by sdebeaulieu on 30/03/15.
 */

sap.ui.controller("demo.view.Menu", {

	onInit: function (evt) {
		//Set localization
		this._localization = new sap.ui.model.resource.ResourceModel({
			bundleUrl: "/i18n/messageBundle.properties"
		});
		sap.ui.getCore().setModel(this._localization, "i18n");

		// set mock model
		var oModel = new sap.ui.model.json.JSONModel({
			"TileCollection": [
				{
					"idPress": "Music",
					"icon": "sap-icon://media-play",
					"title": this._localization.getProperty("music.title")
				},
				{
					"idPress": "Todo",
					"icon": "sap-icon://task",
					"title": this._localization.getProperty("todo.title")
				},
				{
					"idPress": "travel",
					"icon": "sap-icon://travel-itinerary",
					"title": this._localization.getProperty("travel.title")
				}]
		});
		this.getView().setModel(oModel);
	},

	handleEditPress: function (evt) {
		var oTileContainer = this.getView().byId("container");
		var newValue = !oTileContainer.getEditable();
		oTileContainer.setEditable(newValue);
		evt.getSource().setText(newValue ? "Done" : "Edit");
	}
	,

	handleBusyPress: function (evt) {
		var oTileContainer = this.getView().byId("container");
		var newValue = !oTileContainer.getBusy();
		oTileContainer.setBusy(newValue);
		evt.getSource().setText(newValue ? "Done" : "Busy state");
	}
	,

	handleTileDelete: function (evt) {
		var tile = evt.getParameter("tile");
		evt.getSource().removeTile(tile);
	},

	onPress: function (evt) {
		var navto = evt.getSource().data("navto"),
			context = evt.getSource().getBindingContext();
		if (navto) {
			this.nav.to(navto, context);
			console.log("Navigate to " + navto);
		} else {
			console.log("Impossible to navigate, navto undifined");
		}
	}

});
