/**
 * Created by sdebeaulieu on 26/03/15.
 */
sap.ui.controller("demo.view.Music", {

	onInit: function () {
		this.oModel = new sap.ui.model.json.JSONModel({
			//MUSIC PAGE
			newMusicSearch: "",
			itunesResult: {},
			play: {}
		});

		this.getView().setModel(this.oModel);


	},


	/*
	 * **********************************************************************
	 * 						MUSIC METHODS
	 * **********************************************************************
	 */


	onSearchMusic: function () {
		debugger;
		//var url = 'http://localhost:8080/proxy/search?term=' + encodeURI(this.oModel.getProperty("/newMusicSearch"))
		//	+ '&media=music&entity=song';
		var url = 'https://itunes.apple.com/search?term=' + encodeURI(this.oModel.getProperty("/newMusicSearch"))
			+ '&media=music&entity=song&callback=?';

		console.log(url);

		jQuery.getJSON(url, function (data) {
			this.oModel.setProperty("/itunesResult", data);
			this.oModel.setProperty("/newMusicSearch", "");
			this.oModel.refresh();
		}.bind(this));


		/*			jQuery.ajax({
		 url: url,
		 //url: 'resources/testjson.json',
		 method: "GET",
		 dataType: "JSON",
		 xhrFields: {
		 withCredentials: true
		 },
		 success: function (data) {
		 this.oModel.setProperty("/itunesResult", data);
		 console.log(this.oModel);
		 this.oModel.setProperty("/newMusicSearch", "");
		 this.oModel.refresh();
		 }.bind(this)
		 });*/

		//url = encodeURI("http://wsapntwr02.ptx.fr.sopra:8000/sap/opu/odata/sap/ZSERVICE_COUNTRY_SRV/Country?$format=json&$filter=SPRAS eq 'E' and LAND1 eq 'A*' and LANDX eq '*G*'");
		//url = encodeURI("http://localhost/proxy/http/wsapntwr02.ptx.fr.sopra:8000/sap/opu/odata/sap/ZSERVICE_COUNTRY_SRV/Country?$format=json&$filter=SPRAS eq 'E' and LAND1 eq 'A*' and LANDX eq '*G*'");

		/*			console.log(url);
		 jQuery.ajax
		 ({
		 url: url,
		 data: '',
		 dataType: 'JSON',
		 username: "Gmontagne",
		 password: "Sopra123*",
		 contentType:    'application/json',
		 success: function (data){
		 console.log('Essai sopra ws SUCCESS');
		 console.log(data);
		 },
		 error: function(a, b, c) {
		 console.log('Essai sopra ws FAILURE');
		 console.log(a, b, c);
		 console.log(jQuery.parseJSON(a.responseText));

		 }
		 });*/

	},

	onItemPress: function (e) {
		var oModel = e.mParameters.listItem.getBindingContext().oModel,
			playSong = oModel.getProperty(e.mParameters.listItem.getBindingContextPath());
		this.oModel.setProperty("/play", playSong);
		this.oModel.refresh();
	},

	onOpenDialog: function (e) {
		var oModel = e.mParameters.listItem.getBindingContext().oModel,
			playSong = oModel.getProperty(e.mParameters.listItem.getBindingContextPath());
		this.oModel.setProperty("/play", playSong);
		this.oModel.refresh();
		if (!this._oDialog) {
			this._oDialog = sap.ui.xmlfragment("demo.fragments.playerDialog", this);
			this.getView().addDependent(this._oDialog);
		}

		this._oDialog.bindElement("/play");
		// toggle compact style
		jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialog);
		this._oDialog.open();
	},

	onCloseDialog: function (oEvent) {
		jQuery("#trackPlayer")[0].pause();
		this._oDialog.close();
	},

	handleNavButtonPress: function (evt) {
		this.nav.back("Menu");
	}

});


